package ru.t1.azarin.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.service.IJmsConnectionService;
import ru.t1.azarin.tm.listener.EntityListener;
import ru.t1.azarin.tm.log.JmsLoggerProducer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class JmsConnectionService implements IJmsConnectionService {

    @NotNull
    @PersistenceContext
    public EntityManager entityManager;

    @Override
    public void initLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer();
        @NotNull final EntityListener entityListener = new EntityListener(jmsLoggerProducer);

        @NotNull final SessionFactoryImpl sessionFactoryImpl =
                entityManager.getEntityManagerFactory().unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry =
                sessionFactoryImpl.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

}
