package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.dto.IDtoRepository;
import ru.t1.azarin.tm.api.service.dto.IDtoService;
import ru.t1.azarin.tm.dto.model.AbstractDtoModel;

@Service
@NoArgsConstructor
public abstract class AbstractDtoService<M extends AbstractDtoModel, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
