package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.SessionDto;

import java.util.List;

public interface ISessionDtoService {

    void add(@Nullable SessionDto model);

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<SessionDto> findAll(@Nullable String userId);

    @Nullable
    SessionDto findOneById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable SessionDto model);

    void removeById(@Nullable String userId, @Nullable String id);

    void update(@Nullable SessionDto model);

}
