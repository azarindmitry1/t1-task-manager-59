package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.AbstractDtoModel;

public interface IDtoService<M extends AbstractDtoModel> {

    void add(@Nullable M model);

    void remove(@Nullable M model);

    void update(@Nullable M model);

}
