package ru.t1.azarin.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.listener.EntityListener;

public interface IReceiverService {

    void receive(@NotNull EntityListener entityListener);

}
